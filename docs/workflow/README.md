---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Flujo de trabajo para la entrega de actividades
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Flujo de trabajo para la entrega de actividades

![https://drive.google.com/open?id=1_fhDH4NX_2kvMWoRupcdpaOqAATsbw9s](img/000-workflow.png "")

## Acceder al repositorio principal

+ Abrir la URL del [repositorio de tareas para la materia][repositorio-tareas]

|                            |
|:--------------------------:|
| ![](img/001-Main_repo.png)

+ [Iniciar sesión en GitLab][gitlab-login]

|                           |
|:-------------------------:|
| ![](img/002-Sign_in.png)

--------------------------------------------------------------------------------

## Haz `fork` del repositorio principal

+ Dar clic en el botón de `fork` para crear una copia del repositorio `tareas-redes` en tu cuenta de usuario

|                            |
|:--------------------------:|
| ![](img/003-Fork_repo.png)

+ Selecciona tu usuario, marca el repositorio como **público** y espera a que el _fork_ se complete

|                                     |
|:-----------------------------------:|
| ![](img/004-Fork_repo-settings.png)

--------------------------------------------------------------------------------

## Da permisos en tu repositorio

+ Accede a la sección **members** de tu repositorio

|                                    |
|:----------------------------------:|
| ![](img/006-Fork_members-menu.png)

+ Busca el nombre de usuario y da permisos de `maintainer` a los demás integrantes de tu equipo

|                                         |
|:---------------------------------------:|
| ![](img/007-Fork_grant-permissions.png)

!!! note
    Esto no es necesario para la `tarea-0`

--------------------------------------------------------------------------------

## Clona el repositorio

+ Accede a la URL del repositorio `tareas-redes` asociado a **tu cuenta de usuario**

```
https://gitlab.com/USUARIO/tareas-redes.git
```

+ Obten la URL de tu repositorio `tareas-redes` y bájalo a tu equipo con `git clone`

|                                            |
|:------------------------------------------:|
| ![](img/008-Fork_successful-clone_URL.png)

```bash
$ git clone https://gitlab.com/USUARIO/tareas-redes.git
Cloning into 'tareas-redes'...
remote: Enumerating objects: 46, done.
remote: Counting objects: 100% (46/46), done.
remote: Compressing objects: 100% (43/43), done.
remote: Total 46 (delta 2), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (46/46), 3.69 MiB | 1.0 MiB/s, done.
Resolving deltas: 100% (2/2), done.
```

+ Lista el contenido del repositorio y de la carpeta `docs`

```bash
$ cd tareas-redes/
$ ls -lA
total 40
drwxr-xr-x 12 tonejito staff 384 Sep 28 01:02 .git
-rw-r--r--  1 tonejito staff 760 Sep 28 01:02 .gitignore
-rw-r--r--  1 tonejito staff 962 Sep 28 01:02 .gitlab-ci.yml
-rw-r--r--  1 tonejito staff 263 Sep 28 01:02 Makefile
lrwxr-xr-x  1 tonejito staff  14 Sep 28 01:02 README.md -> docs/README.md
drwxr-xr-x  5 tonejito staff 160 Sep 28 01:02 docs
-rw-r--r--  1 tonejito staff 954 Sep 28 01:02 mkdocs.yml
-rw-r--r--  1 tonejito staff 340 Sep 28 01:02 requirements.txt

$ tree -a docs/
docs/
├── README.md -> ../README.md
├── tareas/
│   └── tarea-0
│       ├── README.md
│       └── ...
├── practicas/
│   └── practica-1
│       ├── README.md
│       └── ...
└── workflow/
    ├── README.md
    └── img/
        ├── .gitkeep
        ├── 000-workflow.png
        └── ...
```

--------------------------------------------------------------------------------

## Crear rama de trabajo

+ Revisa que de manera inicial te encuentres en la rama `entregas`

```bash
$ git branch
  entregados
* entregas
  master
```

+ Crea una rama con tu nombre utilizando `git checkout`

```bash
$ git checkout -b AndresHernandez
Switched to a new branch 'AndresHernandez'
```

!!! note
    Para las demás actividades crea una rama que se llame como la actividad (`tarea-1`, `practica-2`, etc.)

    ```
    $ git checkout -b 'practica-1'
    ```

+ Revisa que hayas cambiado a la rama con tu nombre
    - Debe tener el prefijo `*`

```bash
$ git branch
* AndresHernandez
  entregados
  entregas
  master
```

--------------------------------------------------------------------------------

## Agregar carpeta personal

+ Accede al repositorio y crea una carpeta con tu nombre bajo la ruta `docs/tareas`

```bash
$ mkdir -vp docs/tareas/tarea-0/AndresHernandez/
mkdir: created directory ‘docs/tareas/tarea-0/AndresHernandez/’
```

!!! note
    - Dentro de esa carpeta es donde debes poner los archivos de esta tarea.
    - Cada práctica y tarea tendrá su propia carpeta y debes seguir la estructura de directorios propuesta.

--------------------------------------------------------------------------------

### Agregar una imagen

+ Crea el directorio de imagenes

```bash
$ mkdir -vp docs/tareas/tarea-0/AndresHernandez/img/
mkdir: created directory ‘docs/tareas/tarea-0/AndresHernandez/img/’

$ touch docs/tareas/tarea-0/AndresHernandez/img/.gitkeep
```

!!! note
    Se crea el archivo oculto `.gitkeep` para versionar un directorio _"vacío"_, porque `git` no permite versionar directorios

+ Copia una imagen en formato `jpg`, `png` o `gif` dentro del directorio `img`.
    - Asegúrate de que la imagen no pese más de 100KB.

```bash
$ cp  /ruta/hacia/tonejito.png  docs/tareas/tarea-0/AndresHernandez/img/
$ ls -lA docs/tareas/tarea-0/AndresHernandez/img/tonejito.png
-rw-r--r-- 1 tonejito staff 65535 Sep 28 00:10 docs/tareas/tarea-0/AndresHernandez/img/tonejito.png
```

!!! note
    El nombre de tu imagen seguramente es diferente, pero debe estar dentro de la capeta `img`

--------------------------------------------------------------------------------

### Agregar archivo con tu nombre

+ Crea el archivo `.gitkeep` y un archivo de Markdown que contenga tu nombre
    - Agrega una referencia a la imagen que agregaste en el paso anterior para que se muestre dentro del archivo Markdown
    - Puedes agregar otros campos si gustas

```bash
$ touch  docs/tareas/tarea-0/AndresHernandez/README.md
$ editor docs/tareas/tarea-0/AndresHernandez/README.md
```

!!! note
    Reemplaza el comando `editor` con el editor de texto de tu preferencia.
    Puedes visualizar cómo se convierte de Markdown a HTML en algún sitio como [stackedit.io][stackedit]

+ Contenido de ejemplo para el archivo `README.md`
    - Este archivo contiene un título, algunas listas, una liga a una URL externa y una referencia a una imágen

```bash
# Andrés Hernández

- Número de cuenta: `123456789`

Hola, esta es mi carpeta para la [*tarea-0*](https://redes-ciencias-unam.gitlab.io/2022-2/tareas-redes/entrega/tarea-0).

Actividades a las que me quiero dedicar al salir de la facultad:

- Seguridad informática (red team, blue team)
- DevOps (Kubernetes)
- Desarrollo

Cosas que me gusta hacer en mi tiempo libre:

- Dormir con el gato
- Jugar videojuegos
- Ver películas
- Hacer experimentos con componentes electrónicos

| Esta es la imagen que elegí |
|:---------------------------:|
| ![](img/tonejito.png)       |
```

!!! note
    El nombre de tu imagen seguramente es diferente, pero debe estar dentro de la capeta `img`

--------------------------------------------------------------------------------

## Envía los cambios a tu repositorio

+ Una vez que hayas creado los archivos, revisa el estado del repositorio

```bash
$ git status
On branch AndresHernandez
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	docs/tareas/tarea-0/AndresHernandez/

nothing added to commit but untracked files present (use "git add" to track)
```

+ Arega los archivos con `git add`

```bash
$ git add docs/tareas/tarea-0/AndresHernandez/
```

+ Revisa que los archivos hayan sido agregados al _staging area_ utilizando `git status`

```bash
$ git status
On branch AndresHernandez
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   docs/tareas/tarea-0/AndresHernandez/README.md
	new file:   docs/tareas/tarea-0/AndresHernandez/img/.gitkeep
	new file:   docs/tareas/tarea-0/AndresHernandez/img/tonejito.png
```

+ Versiona los archivos con `git commit`

!!! warning
    Usa **comillas simples** para especificar el mensaje del _commit_

```bash
$ git commit -m 'Carpeta de Andrés Hernández'
[AndresHernandez cb7fe99] Carpeta de Andrés Hernández
 3 files changed, 22 insertions(+)
 create mode 100644 docs/tareas/tarea-0/AndresHernandez/README.md
 create mode 100644 docs/tareas/tarea-0/AndresHernandez/img/.gitkeep
 create mode 100644 docs/tareas/tarea-0/AndresHernandez/img/tonejito.png
```

+ Revisa que el _remote_ apunte a **tu repositorio** con `git remote`

```bash
$ git remote -v
origin	https://gitlab.com/USUARIO/tareas-redes.git (fetch)
origin	https://gitlab.com/USUARIO/tareas-redes.git (push)
```

+ Revisa la rama en la que estas para enviarla a GitLab

```bash
$ git branch
* AndresHernandez
  entregados
  entregas
  master
```

+ Envía los cambios a **tu repositorio** utilizando `git push`

```bash
$ git push -u origin AndresHernandez
Username for 'https://gitlab.com': USUARIO
Password for 'https://USUARIO@gitlab.com':
Enumerating objects: 12, done.
Counting objects: 100% (12/12), done.
Delta compression using up to 12 threads
Compressing objects: 100% (7/7), done.
Writing objects: 100% (10/10), 63.63 KiB | 1.0 MiB/s, done.
Total 10 (delta 1), reused 1 (delta 0), pack-reused 0
remote:
remote: To create a merge request for AndresHernandez, visit:
remote:   https://gitlab.com/USUARIO/tareas-redes/-/merge_requests/new?merge_request%5Bsource_branch%5D=AndresHernandez
remote:
To https://gitlab.com/USUARIO/tareas-redes.git
 * [new branch]      AndresHernandez -> AndresHernandez
Branch 'AndresHernandez' set up to track remote branch 'AndresHernandez' from 'origin'.
```

--------------------------------------------------------------------------------

## Crea un _merge request_ para entregar tu actividad

Para integrar las tareas de todos se utilizará la funcionalidad `merge request` de GitLab

+ Accede a la url que te apareció en el paso anterior

```
https://gitlab.com/USUARIO/tareas-redes/-/merge_requests/new?merge_request%5Bsource_branch%5D=NOMBRE
```

+ Verifica que la rama de destino sea la rama de la actividad que vas a entregar (`tarea-1`, `practica-2`, etc.)
    - Da clic en el botón `Change branches` para ajustar si es necesario

|                                         |
|:---------------------------------------:|
| ![](img/009-Fork-MR_change-branches.png)

+ Cambia a la rama de la actividad que vas a entregar (`tarea-1`, `practica-2`, etc.)

|                                       |
|:-------------------------------------:|
| ![](img/009-Fork-MR_select-branch.png)


+ Llena los datos del merge request
    - Escribe un título y una descripción que sirva como vista previa para tu entrega
    - Asegúrate de dejar las siguientes casillas **sin marcar**
        * `[ ]` _Delete source branch when merge request is accepted_.
        * `[ ]` _Squash commits when merge request is accepted_.
+ Da clic en el botón _submit_ para crear tu _merge request_

|                               |
|:-----------------------------:|
| ![](img/009-Fork-MR_data.png)

### Estado del pipeline

Este [repositorio de tareas][repositorio-tareas] cuenta con una configuración de CI/CD que publica el contenido de la carpeta `docs` en [un sitio web][repositorio-vista-web].
Esta operación se realiza mediante un _pipeline_ que construye el sitio y que puede tener uno de 4 estados

| Estado | Acción |
|:------:|:-------|
| <span class="gold text-border">pending</span> | Esperar a que el _pipeline_ se ejecute
| <span class="blue">running</span> | Esperar a que el _pipeline_ termine su ejecución
| <span class="green">passed</span> | El trabajo de CI/CD fue exitoso y el _merge request_ podrá ser revisado y aprobado
| <span class="red">failed</span>   | Revisar los mensajes de la bitácora y corregir errores

!!! note
    - Normalmente los errores del pipeline se dan por archivos que se referencían pero no existen.
    - Verificar los nombres de archivo y las ligas a imágenes y archivos complementarios.
    - Para más información consulta el documento [`debug.md`](debug.md)

--------------------------------------------------------------------------------

##### Notificaciones de creación y seguimiento del _merge request_

+ Una vez que hayas enviado el `merge request`, le llegará un correo electrónico al responsable para que integre tus cambios y podrás visualizar la revisión en la liga
    - <https://gitlab.com/Redes-Ciencias-UNAM/2022-2/tareas-redes/-/merge_requests>

<!--
|                                  |
|:--------------------------------:|
| ![](img/010-Main_MR_created.png)
-->

+ Cuando se hayan integrado tus cambios te llegará un correo electrónico de confirmación y aparecerá en el panel del [repositorio principal][repositorio-tareas]

|                                 |
|:-------------------------------:|
| ![](img/012-Main_MR_merged.png)

--------------------------------------------------------------------------------

[repositorio-tareas]: https://gitlab.com/Redes-Ciencias-UNAM/2022-2/tareas-redes.git
[repositorio-vista-web]: https://redes-ciencias-unam.gitlab.io/2022-2/tareas-redes/
[gitlab-login]: https://gitlab.com/users/sign_in
[repositorio-personal]: https://gitlab.com/USUARIO/tareas-redes
[stackedit]: https://stackedit.io/app#
